/*
 * Copyright (c) 2021 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <getopt.h>
#include <rte_ethdev.h>
#include <doca_flow.h>
#include <utils.h>
#include <flow_offload.h>
DOCA_LOG_REGISTER(DNS_FILTER);

/*
 * the number of TSC cycles to pass between
 * each two aggregatations of DNS packets
 */
#define TSC_CYCLES_LIMIT 1000000000
#define PACKET_BURST 32 /* the number of packets in the rx queue */
#define DNS_PORT 53
#define MAX_PORT_STR 128

static bool force_quit;

enum app_args {
	ARG_HELP = 'h',
	ARG_LOG_LEVEL = 'l',
};

static void
handle_packets_received(uint16_t packets_received,
			struct rte_mbuf **packets, int *dns_count)
{
	struct rte_mbuf *packet = NULL;
	uint16_t queue_id = 0;
	uint8_t ingress_port;
	uint32_t current_packet;

	for (current_packet = 0;
	current_packet < packets_received; current_packet++) {

		packet = packets[current_packet];

		/* deciding the port to send the packet to */
		ingress_port = packet->port^1;

		print_l4_header(packet);
	}
	/* packet sent to port 0 or 1*/
	rte_eth_tx_burst(ingress_port,
	queue_id, packets, packets_received);

}

static void
process_packets(unsigned int nb_queues, unsigned int nb_ports)
{
	struct rte_mbuf *packets[PACKET_BURST];
	int dns_count = 0;
	int dns_count_last = 0;
	uint64_t stc_time = rte_get_tsc_cycles();
	uint16_t nb_packets, queue;
	uint8_t ingress_port;

	while (!force_quit) {
		for (ingress_port = 0; ingress_port < nb_ports;
		ingress_port++) {
			for (queue = 0; queue < nb_queues; queue++) {
				/* get number of packets received on rx queue */
				nb_packets = rte_eth_rx_burst(ingress_port,
				queue, packets, PACKET_BURST);

				/* check if packets received and handle them */
				if (nb_packets) {
					handle_packets_received(nb_packets,
					packets, &dns_count);
				}
			}

		/* aggregate DNS packets every TSC_CYCLES_LIMIT
		 * cycles and update the counters and stc_time
		 */
		if (rte_get_tsc_cycles() - stc_time > TSC_CYCLES_LIMIT) {
			stc_time = rte_get_tsc_cycles();
			if (dns_count != dns_count_last)
				dns_count_last = dns_count;
			}
		}
	}

}

/* builds dns flow pipe for every port */
static void
build_dns_pipe(struct doca_flow_port *port, int nb_queues)
{
	struct doca_flow_match dns_match;
	struct doca_flow_fwd dns_fw;
	/**< match mask for the pipeline*/
	struct doca_flow_match match_mask;
	/**< actions for the pipeline*/
	struct doca_flow_actions actions;
	/**< monitor for the pipeline*/
	struct doca_flow_monitor monitor;
	struct doca_flow_pipe_cfg dns_pipe_cfg;
	struct doca_flow_pipe *dns_pipe;
	struct doca_flow_error err = {0};
	uint16_t rss_queues[nb_queues];
	int queue_index;
	struct doca_flow_pipe_entry *entry;

	/* allocate DNS pipe fields */
	memset(&actions, 0, sizeof(actions));
	memset(&monitor, 0, sizeof(monitor));
	memset(&dns_fw, 0, sizeof(dns_fw));
	memset(&match_mask, 0, sizeof(match_mask));
	memset(&dns_match, 0, sizeof(dns_match));
	memset(&dns_pipe_cfg, 0, sizeof(dns_pipe_cfg));

	dns_pipe_cfg.name = "DNS_PIPE";
	dns_pipe_cfg.match = &dns_match;
	dns_pipe_cfg.port = port;
	dns_pipe_cfg.match_mask = &match_mask;
	dns_pipe_cfg.actions = &actions;
	dns_pipe_cfg.monitor = &monitor;

	dns_match.out_dst_ip.type = DOCA_FLOW_IP4_ADDR;
	dns_match.out_l4_type = IPPROTO_UDP;
	dns_match.out_dst_port = rte_cpu_to_be_16(DNS_PORT);
	match_mask.out_dst_port = 0xffff;

	/* configure queues for rss fw */
	for (queue_index = 0; queue_index < nb_queues; queue_index++)
		rss_queues[queue_index] = queue_index;

	dns_fw.type = DOCA_FLOW_FWD_RSS;
	dns_fw.rss_queues = rss_queues;
	dns_fw.rss_flags = ETH_RSS_UDP;
	dns_fw.num_of_queues = nb_queues;

	dns_pipe = doca_flow_create_pipe(&dns_pipe_cfg, &dns_fw, &err);
	if (dns_pipe == NULL)
		APP_EXIT("DNS pipe creation FAILED: %s\n", err.message);

	/* add HW offload dns rule */
	entry = doca_flow_pipe_add_entry(0, dns_pipe, &dns_match,
			&actions, &monitor, &dns_fw, &err);
	if (entry == NULL)
		APP_EXIT("entry creation FAILED: %s\n", err.message);

}

static struct doca_flow_pipe_entry*
hairpin_non_dns_packets(struct doca_flow_port *port, uint16_t port_id,
		 uint16_t hairpin_queue, int l4_type)
{
	struct doca_flow_match non_dns_match;
	struct doca_flow_fwd non_dns_fw;
	/**< match mask for the pipeline*/
	struct doca_flow_match match_mask;
	/**< actions for the pipeline*/
	struct doca_flow_actions actions;
	/**< monitor for the pipeline*/
	struct doca_flow_monitor monitor;
	struct doca_flow_pipe_cfg non_dns_pipe_cfg;
	struct doca_flow_pipe *non_dns_pipe;
	struct doca_flow_error err = {0};
	struct doca_flow_pipe_entry *entry;

	/* zeroed fields are ignored , no changeable fields */
	memset(&non_dns_match, 0, sizeof(non_dns_match));
	memset(&match_mask, 0, sizeof(match_mask));
	memset(&actions, 0, sizeof(actions));
	memset(&monitor, 0, sizeof(monitor));
	memset(&non_dns_fw, 0, sizeof(non_dns_fw));
	memset(&non_dns_pipe_cfg, 0, sizeof(non_dns_pipe_cfg));

	non_dns_pipe_cfg.match = &non_dns_match;

	non_dns_match.out_dst_ip.type = DOCA_FLOW_IP4_ADDR;
	/* configure the port id "owner" of pipe */
	non_dns_pipe_cfg.port = port;
	non_dns_pipe_cfg.match_mask = &match_mask;
	non_dns_pipe_cfg.actions = &actions;
	non_dns_pipe_cfg.monitor = &monitor;
	switch (l4_type) {
	case IPPROTO_TCP:
		non_dns_pipe_cfg.name = "TCP_NON_DNS_PIPE";
		non_dns_match.out_l4_type = IPPROTO_TCP;
		break;
	case IPPROTO_UDP:
		non_dns_pipe_cfg.name = "UDP_NON_DNS_PIPE";
		non_dns_match.out_l4_type = IPPROTO_UDP;

	}

	non_dns_fw.type = DOCA_FLOW_FWD_RSS;
	non_dns_fw.rss_queues = &hairpin_queue;
	non_dns_fw.rss_flags = ETH_RSS_IP;
	non_dns_fw.num_of_queues = 1;

	non_dns_pipe = doca_flow_create_pipe(&non_dns_pipe_cfg,
		&non_dns_fw, &err);
	if (non_dns_pipe == NULL)
		APP_EXIT("failed to create non-DNS pipe: %s\n", err.message);

	entry = doca_flow_pipe_add_entry(0, non_dns_pipe, &non_dns_match,
		&actions, &monitor, &non_dns_fw, &err);
	return entry;
}

static void
build_hairpin_pipes(struct doca_flow_port *port, uint16_t port_id,
	 uint16_t reserved_hairpin_queue)
{
	struct doca_flow_error err = {0};
	struct doca_flow_pipe_entry *hairpin_flow;

	hairpin_flow = hairpin_non_dns_packets(port, port_id,
			reserved_hairpin_queue, IPPROTO_UDP);
	if (hairpin_flow == NULL)
		APP_EXIT("Hairpin UDP flow creation failed: %s\n", err.message);

	hairpin_flow = hairpin_non_dns_packets(port, port_id,
			reserved_hairpin_queue, IPPROTO_TCP);
	if (hairpin_flow == NULL)
		APP_EXIT("Hairpin TCP flow creation failed: %s\n", err.message);

}

/* initialize doca flow ports */
struct doca_flow_port *
dns_filter_port_init(struct doca_flow_port_cfg *port_cfg, uint8_t portid)
{
	char port_id_str[MAX_PORT_STR];
	struct doca_flow_error err = {0};
	struct doca_flow_port *port;

	memset(port_cfg, 0, sizeof(*port_cfg));
	port_cfg->port_id = portid;
	port_cfg->type = DOCA_FLOW_PORT_DPDK_BY_ID;
	snprintf(port_id_str, MAX_PORT_STR, "%d", port_cfg->port_id);
	port_cfg->devargs = port_id_str;

	port = doca_flow_port_start(port_cfg, &err);
	if (port == NULL)
		APP_EXIT("failed to initialize doca flow port: %s\n",
			err.message);
	return port;
}

static void
usage(const char *prog_name)
{
	printf("%s [EAL options] --\n"
		"-l or --log_level: Set the log level for the app ERR=0, DEBUG=3\n",
		prog_name);
}


static void
parse_input_args(int argc, char **argv)
{
	char **argvopt;
	int opt, opt_idx;
	static struct option lgopts[] = {
		/* Show help */
		{ "help",  no_argument, 0, ARG_HELP},
		/* Log level */
		{ "log_level",  required_argument, 0, ARG_LOG_LEVEL},
		/* End of option */
		{ 0, 0, 0, 0 }
	};
	static const char *shortopts = "hc:o:pnitl:";

	argvopt = argv;
	while ((opt = getopt_long(argc, argvopt, shortopts,
			lgopts, &opt_idx)) != EOF) {
		switch (opt) {

		case ARG_LOG_LEVEL:
			doca_log_global_level_set(atoi(optarg));
			break;
		case ARG_HELP:
			usage("DNS filter example app");
			break;
		default:
			fprintf(stderr, "Invalid option: %s\n", argv[optind]);
			usage("DNS filter example app");
			APP_EXIT("Invalid option\n");
			break;
		}
	}
}

void
dns_filter_close_port(uint16_t port_id)
{
	struct rte_flow_error error = {0};
	int ret;

	doca_flow_destroy_port(port_id);
	ret = rte_flow_flush(port_id, &error);
	if (ret < 0)
		APP_EXIT("Failed to close and release resources: %s\n",
				error.message);

	rte_eth_dev_stop(port_id);
	rte_eth_dev_close(port_id);
}

/*
 *  The main function, which does initialization
 *  of the rules and starts the process of filtering the DNS packets.
 */
int
main(int argc, char **argv)
{
	unsigned int nb_queues, nb_ports;
	struct doca_flow_error err = {0};
	struct doca_flow_cfg dns_flow_cfg;
	struct doca_flow_port **ports;
	struct doca_flow_port_cfg port_cfg;
	uint16_t portid, peer_ports;

	/* dpdk initialization */
	dpdk_init(&argc, &argv, &nb_queues, &nb_ports);

	if (argc > 1)
		parse_input_args(argc, argv);

	ports = (struct doca_flow_port **)
		malloc(sizeof(struct doca_flow_port *) * nb_ports);
	if (!ports)
		APP_EXIT("cannot allocate memory for doca flow ports");

	if (dpdk_ports_init(nb_ports, nb_queues) != 0)
		rte_exit(EXIT_FAILURE, "Ports allocation failed\n");

	/* enable hairpin for each port */
	for (portid = 0; portid < nb_ports; portid++) {
		if (enable_hairpin_queues(portid, &peer_ports, 1) != 0)
			APP_EXIT("Hairpin bind failed");
	}

	/* initialize doca framework */
	dns_flow_cfg.total_sessions = nb_ports;
	dns_flow_cfg.queues = nb_queues;
	dns_flow_cfg.is_hairpin = true;

	if (doca_flow_init(&dns_flow_cfg, &err))
		APP_EXIT("failed to init doca: %s\n", err.message);

	for (portid = 0; portid < nb_ports; portid++) {
		/* initialize doca flow port */
		ports[portid] = dns_filter_port_init(&port_cfg, portid);

		/* hairpin pipes for non-dns packets */
		build_hairpin_pipes(ports[portid], portid, nb_queues);

		/* dns flow pipe */
		build_dns_pipe(ports[portid], nb_queues-1);
	}

	/* process packets */
	process_packets(nb_queues, nb_ports);

	/* closing and releasing resources */
	for (portid = 0; portid < nb_ports; portid++)
		dns_filter_close_port(portid);

	return 0;
}
